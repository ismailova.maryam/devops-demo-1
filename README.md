# Demo-1 for DevOps course
## Automating application startup using Vagrant
### Table of contents: 
* [Introduction](#introduction)
* [To-do list](#to-do-list)
* [Technologies used](#technologies-used)
* [Launching](#launching)
* [Details](#details)
### Introduction
The goal of this project was, provided a project and its requirements, to write a Vagrantfile in order to provision Virtul Machines with all dependencies installed and to startup application.

The end goal was for an application in VM APP_VM to be able to access a database on VM DB_VM with its user.

Some checks for health of application and database have beed added.
### To-do list
- [x] VMs for spring boot application and a mysql database
- [x] Private network between APP_VM and DB_VM
- [x] Installing MYSQL db and creating database and user, with restrictions to specific subnet
- [x] Installing jdk, cloning app and mvn packaging
- [x] Providing environment variables to VMs for configurations
- [x] Running application under appuser profile
- [x] Executing database healthchecks
- [x] Executing application healthcheck on provisioning end
- [x] Executing continuous database healthchecks with possibility to fallback
- [ ] Deploying on AWS

### Technologies used
[hashicorp/bionic64](https://app.vagrantup.com/hashicorp/boxes/bionic64 "hashicorp/bionic64") box which has image of standard Ubuntu 18.04 LTS 64-bit

Vagrant for VM creation and provisioning (check Vagrantfile)

Vagrant caching plugin vagrant-cachier in order to cache the the results of package managers.

Bash scripts for provisioning and application startup

### Launching
0. Verify that **Vagrant** is installed on your machine. 
1. Clone this repository:
    
    `git clone https://gitlab.com/ismailova.maryam/devops-demo-1.git`
2. Set the environment variables on your machine prior to launching vagrant provisioning. 

    Required variables are:
    - **GITTOKEN** <- this is a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html "Gitlab Personal access tokens") that you should have for the following [project](https://gitlab.com/dan-it/az-groups/az-devops1) in order to clone seamlessly from that repository.
    - **MYSQL_USER** <- the mysql username for the database (EX: "mysqluser")
    - **MYSQL_URL** <- The url towards your machine hosting database (Dont forget to set the same ip as the one used in Vagrantfile for DB_VM) (EX: jdbc:mysql://192.168.33.14/petclinic)
    - **MYSQL_PASS** <- Password for the mysql user (EX: "12345678")
    - **FALLBACK** <- Variable which is used for database fallback decision making. FALLBACK=0 means that fallback will be used. Any other value will disable FALLBACK.
3. Verify that **IPs 192.168.33.14 (for db)** and **192.168.33.15 (for app)** are not used on your machine. Otherwise you would need to change them in Vagrantfile (_lines X.vm.network "private_network", ip: "<IP>"_) and also to modify your **MYSQL_URL** variable.
4. Go to project directory and launch VM provisioning through:
    
    `vagrant up`

    And wait for following message to appear:
    'App is up and running!'
    
    If not, please check the last message and go through [Details section](#details)

### Details:

During the first launch provisioning might take quite a while due to maven dependencies **(~40min)** being downloaded. 
However this process has been optimized, and the **local m2 repository** will be synced to your project directory(.m2) and will be reused during following provisionings. 

What is also worth noting, is that eventhough scripting was used for provisioning it is as idempotent as possible. And therefor even running `vagrant reload --provision` will not break anything. Nevertheless the logic of Vagrantfile was written in such a way that scripts that are only needed once are not being executed during reloading by default(EX: ./setupapp.sh, ./setupdb.sh), and those that are important for startup are run everytime you restart the application (./startapp.sh, + application healthcheck)

First database machine will be provisioned and next application VM.

In order to deal with time settings timezone has been set to Asia/Baku on both machines( `timedatectl set-timezone Asia/Baku` ). Please refer to your timzeone if it varies, as in some situation this might cause problems during provisioning(package managers won't work).

When repository is cloned(or pulled) and packaged, script checks for the exit status of maven test. And if tests fail the provisioning will stop and fail. (check ./setupapp.sh script).
Otherwise Vagrant will try to launch the application.

Launching of application is called from ./startapp.sh script everytime you start/reload your app machine. Logic of app launching is following:

* Database healthcheck is executed and if it is available and application is not yet running it will be started with mysql profile.
* If MySQL database is available but application is running with a different profile, application will be restarted with mysql profile.
* If MySQL database won't be available but FALLBACK would be enabled(FALLBACK=0) then application will either be started or restarted with default profile(H2 database).
* However if FALLBACK is not enabled and MySQL is not available, provisioning will fail!

Detailed startup logic can be seen in **./scripts_to_copy/dbhealthcheck.sh** script. This script is called at startup, but also through a cronjob.

A cronjob for appuser user is created at startup in order to check for database availability every 2 minutes and to make a decision for fallback strategy.

When application is started there is one last healthcheck script executed that waits for 2 minutes for application to start. If everything is successful, the message 'App is up and running!' will be displayed, otherwise the application startup took too long and it means that your provisioning did not go as expected or VMs are malfunctioning.

`curl  -s --retry 40 --retry-connrefused --retry-delay 3 --retry-max-time 120 192.168.33.15:8080/actuator/health | grep -q "{\"status\":\"UP\"}"`

This comand is used for application healthcheck. 