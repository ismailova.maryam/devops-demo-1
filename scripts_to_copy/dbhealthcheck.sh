#!/bin/bash
echo "`date`:"
if  mysql -h 192.168.33.14 -u $MYSQL_USER -p$MYSQL_PASS  -e "show databases;" 2>/dev/null| grep -q petclinic; then
    echo "MySQL is available"
    if ! pgrep -u appuser -f 'java -jar' &>/dev/null 
    then
        echo "No version of application is running. Going to start it with MySQL profile"
        java -jar -Dspring.profiles.active=mysql *.jar &> applogs.log &
    else if pgrep -u appuser -f 'java -jar spring' &>/dev/null 
    then 
        echo "H2 version is running. Will restart it with MySql"
        pkill -u appuser -f java &> /dev/null && java -jar -Dspring.profiles.active=mysql *.jar &> applogs.log &
    fi
    fi

else
    echo "MySQL db is unavailable"
    if [ $FALLBACK -ne 0 ]; then echo "No fallback. Provisioning failed!"; exit 3; fi
    
    if ! pgrep -u appuser -f 'java -jar' &>/dev/null 
    then
        echo "No version of application is running. Going to start it with H2"
        java -jar *.jar &> applogs.log &
    else if pgrep -u appuser -f 'java -jar -Dspring.profiles.active=mysql' &>/dev/null 
    then 
        echo "MySQL version is running. Will restart it with H2"
        pkill -u appuser -f java &> /dev/null && java -jar *.jar &> applogs.log &
    fi
    fi
fi
