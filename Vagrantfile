#PS: Use vagrant ssh APP_VM -c "sudo pkill java" to stop APP_VM machine, since otherwise vagrant fails to stop it
# By default it uses database fallback method, if mysql db is not detected. (FALLBACK='0')
# In order to change it, and stop provisioning if db is not up, change FALLBACK env variable to 1

Vagrant.configure("2") do |config|
    config.vm.box = "hashicorp/bionic64"
    config.vm.boot_timeout = 300
    config.vm.usable_port_range = 8000..9000
    config.vm.provision :shell, inline: "timedatectl set-timezone Asia/Baku"
    config.vagrant.plugins=["vagrant-cachier"]
    # No need to copy all the files from project folder
    config.vm.synced_folder ".", "/vagrant", disabled: true

    # Use caching for faster updates
    if Vagrant.has_plugin?("vagrant-cachier")
        config.cache.scope = :box
    end

    config.vm.define "DB_VM" do |db|
        db.vm.network "private_network", ip: "192.168.33.14"

        db.vm.provision :shell, path: "setupdb.sh", env: {'MYSQL_USER' =>ENV['MYSQL_USER'], 'MYSQL_URL' => ENV['MYSQL_URL'], 'MYSQL_PASS' => ENV['MYSQL_PASS']}

        # Requirements for VM of DB
        db.vm.provider "virtualbox" do |vb|
            vb.name = "DB_VM"
            vb.memory = "3072"
        end
    end

    config.vm.define "APP_VM" do |app|
        # Sync m2 local repository in order to increase the speed of startup from base
        app.vm.synced_folder "./.m2/", "/root/.m2/", create: true
        app.vm.synced_folder "./scripts_to_copy", "/vagrant"
        # Setup all the environement for app
        app.vm.provision :shell, path: "setupapp.sh", env: {'GITTOKEN' =>ENV['GITLABTOKEN'], 'MYSQL_USER' =>ENV['MYSQL_USER'], 'MYSQL_URL' => ENV['MYSQL_URL'], 'MYSQL_PASS' => ENV['MYSQL_PASS']}
        # Start the java app everytime the VM is started (runs everytime)
        app.vm.provision :shell, path: "startapp.sh" , run: "always", env: {'FALLBACK' => ENV['FALLBACK']}
        # Execute an application check (runs everytime)
        app.vm.provision :shell, inline: 'echo "Checking if application is up"; if ! curl  -s --retry 40 --retry-connrefused --retry-delay 3 --retry-max-time 120 192.168.33.15:8080/actuator/health | grep -q "{\"status\":\"UP\"}"; then  echo "App is not in healthy mode" && pkill java && exit 3; else  echo "App is up and running!"; fi', run: "always"
        app.vm.network "private_network", ip: "192.168.33.15"
        app.vm.network "forwarded_port", guest:8080, host:8999, auto_correct: true

        # Requirements for VM of DB
        app.vm.provider "virtualbox" do |vb|
            vb.name = "APP_VM"
            vb.memory = "3072"
        end
    end

end


