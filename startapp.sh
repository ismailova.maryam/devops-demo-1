#!/bin/bash

# Set or Replace FALLBACK env.var
# Description: '! grep -q ..... ' checks if FALLBACK is already defined in file and returns true if not found (! to negate). And if true (&&), FALLBACK variable will be exported into profile
# If however FALLBACK variable is already defined, then '! grep..' will be false, and '|| sed..' will be executed, thus updating the value of exported variable if it changes 
! grep -q 'FALLBACK=' /home/appuser/.profile && echo "export FALLBACK=$FALLBACK" >> /home/appuser/.profile || sed -i "s/\(FALLBACK=\)\(.*\)/\1$FALLBACK/" /home/appuser/.profile

cd /home/appuser 

# Start application with appuser user
# dbhealthcheck script checks database availablility, and based on fallback policy starts/restarts app with working profile
sudo -iu appuser ./dbhealthcheck.sh || exit 2

#Setup healthcheck cron to fallback to available database (with default being MySQL)
# This cronjob will continuously check for database and fix application profile if needed
if ! $(crontab -u appuser -l | grep -q " /home/appuser/dbhealthcheck.sh " ) 
then
    echo "Setting the cronjob for DB fallback"
    (crontab -l -u appuser 2> /dev/null ; echo "*/2 * * * *  sudo -iu appuser /home/appuser/dbhealthcheck.sh >> /home/appuser/healthcheck.log") | crontab -u appuser -
fi
exit 0
