#!/bin/bash

# Export variables
if ! grep -q MYSQL /etc/profile
then
echo "
export MYSQL_USER="$MYSQL_USER"
export MYSQL_URL="$MYSQL_URL"
export MYSQL_PASS="$MYSQL_PASS"
" >> /etc/profile
fi

# Install mysql server and client
sudo apt-get update -y 
sudo apt-get install mysql-server -y

# Configure mysql database and user
sudo mysql << EOF
    CREATE USER '$MYSQL_USER'@'192.168.33.%' IDENTIFIED BY '$MYSQL_PASS';
    CREATE DATABASE IF NOT EXISTS petclinic;

    ALTER DATABASE petclinic
        DEFAULT CHARACTER SET utf8
        DEFAULT COLLATE utf8_general_ci;

    GRANT ALL PRIVILEGES ON petclinic.* TO '$MYSQL_USER'@'192.168.33.%' IDENTIFIED BY '$MYSQL_PASS';
EOF

# Set mysql to listen to all connections, not only from localhost
sudo sed -i 's/\(bind-address.*\)=\(.*\)/\1= 0.0.0.0/'  /etc/mysql/mysql.conf.d/mysqld.cnf

sudo systemctl restart mysql