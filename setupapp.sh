#!/bin/bash

# Create a user
useradd -m -s /bin/bash -c 'APP_USER' appuser
usermod -aG sudo appuser

# Install Java 11
apt-get update -y
apt-get install openjdk-11-jre mysql-client -y

# Clone the repository from git and package it
if [ -d ./az-devops1 ]
then 
    echo "az-devops1 project directory already exists. Going to update it with pull"
    cd az-devops1
    git pull
else
    echo "az-devops1 project directory does not exist. It will be cloned from git"
    git clone https://oauth2:$GITTOKEN@gitlab.com/dan-it/az-groups/az-devops1.git
    cd az-devops1
fi

chmod +x mvnw
./mvnw clean package 

# If maven packaging was successful then copy jars to appuser, otherwise break provisioning
if [ "$?" -eq 0 ]
then 
    cp target/*.jar /home/appuser/
    chown appuser:appuser /home/appuser/*.jar
else
    exit 2
fi

# Set env vars if not set already
if ! grep -q MYSQL /home/appuser/.profile
then
echo "
export MYSQL_USER="$MYSQL_USER"
export MYSQL_URL="$MYSQL_URL"
export MYSQL_PASS="$MYSQL_PASS"
" >> /home/appuser/.profile
fi

# Copy dbhealthcheck script to appuser home dir
cp /vagrant/dbhealthcheck.sh /home/appuser/
chmod +x /home/appuser/dbhealthcheck.sh
chown appuser:appuser /home/appuser/dbhealthcheck.sh